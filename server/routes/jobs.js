const express = require('express');
const router = express.Router();

function handleError(err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

module.exports = function (Jobs) {
    //get a list of job titles
    router.get("/title", function (req, res) {
        const fields = ["title"];
        Jobs.findAll({
            attributes: fields
        }
        ).then(jobs => {
            res.status(200).json(jobs);
        })
    });

    return router;
}