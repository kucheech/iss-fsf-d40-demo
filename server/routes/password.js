const express = require('express');
const router = express.Router();
const pwHelper = require("../helpers/password");
const uuid = require("uuid/v4");
const handleError = require("../helpers/handleError");


// function handleError(err, res) {
//     res.status(500).type("text/plain").send(JSON.stringify(err));
// }

module.exports = function (Users, Verification, config, mailgun) {

    router.post("/change", function (req, res) {
        const id = req.body.id;
        const password = req.body.password;
        // console.log(id + " " + password);

        Verification.findOne({ where: { id: id } }).then(found => {
            // console.log(found)
            if (found) {
                Users.update({ password: pwHelper.getHash(password) }, { where: { email: found.email } }).then(result => {
                   //delete the instance and then send email.
                    Verification.destroy({ where: { id: found.id } }).then(result => {
                        res.status(200).send("Password changed successfully. Please proceed to login");
                    }).catch(function (err) {
                        // console.log(err);
                        // res.status(500).send(err);
                        handleError(err, res);
                    });
                }).catch(err => {
                    // console.log(err);
                    // res.status(500).send(err);
                    handleError(err, res);
                });
            } else {
                res.status(404).send("Invalid or expired reset credentials");
            }

        }).catch(err => {
            console.log(err);
            // res.status(500).send(err);
            handleError(err, res);
        });
    });

    router.post("/reset", function (req, res) {
        const email = req.body.email;

        var obj = {
            id: uuid(),
            email: email
        }
        Verification.create(obj).then(created => {
            console.log("verification instance inserted for " + email)
        }).catch(err => {
            console.log(err);
            handleError(err, res);
        });

        var text = "\n\n\nWe have received a request to reset your password.";
        const link = "http://" + config.get("host") + "/changepassword/" + obj.id;
        text += "\nPlease click on the following link to change password: " + link;
        text += "\n\n\nIf you did not make this request or believe this was received in error, please ignore this message."
        Users.findOne({ where: { email: email } })
            .then(found => {
                if (found) {
                    var data = {
                        from: "donotreply@wkupteltd.com",
                        to: email,
                        subject: "Reset password",
                        text: "Hi " + found.first_name + "," + text
                    };

                    mailgun.messages().send(data, function (error, body) {
                        if (error) {
                            console.log(error);
                            console.log(body);
                        } else {
                            console.log(body);
                            res.status(200).send("Reset password email has been sent to " + email);
                        }
                    });
                } else {
                    res.status(400).send("User does not exist in database");
                }
            }).catch(err => {
                handleError(err, res);
            });

    });

    return router;
}
