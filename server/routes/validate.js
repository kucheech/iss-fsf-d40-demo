const express = require('express');
const router = express.Router();

function handleError(err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

module.exports = function (Users) {

    //find if a particular email exists
    router.get("/email/:email", function (req, res) {
        const email = req.params.email;
        Users.findOne({
            where: { email: email }
        }).then(user => {
            if (user) {
                res.status(302).send("Email exists in database");
            } else {
                res.status(200).end("Email does not exists in database");
            }
        }).catch(err => {
            handleError(err, res);
        });
    });

    return router;
}