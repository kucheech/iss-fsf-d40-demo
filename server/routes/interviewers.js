const express = require('express');
const router = express.Router();
const handleError = require("../helpers/handleError");

// function handleError(err, res) {
//     res.status(500).type("text/plain").send(JSON.stringify(err));
// }

module.exports = function (Users) {
    //get all interviewers
    router.get("/", function (req, res) {
        const fields = ["id", "first_name", "family_name"];
        Users.findAll({
            where: { role: 2 },
            attributes: fields
        }
        ).then(users => {
            res.status(200).json(users);
        })
    });

    return router;
}