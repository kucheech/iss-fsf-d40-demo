const express = require("express");
const router = express.Router();
const handleError = require("../helpers/handleError");
const pwHelper = require("../helpers/password");

module.exports = function (Users, Candidates, mailgun) {

    //get all users
    router.get("/", function (req, res) {
        const fields = ["first_name", "family_name", "email", "contact", "role"];
        Users.findAll({
            attributes: fields
        }
        ).then(users => {
            res.status(200).json(users);
        })
    });

    //get a particular user based on id
    router.get("/:id", function (req, res) {
        const id = req.params.id;
        const fields = ["first_name", "family_name", "email", "contact", "role"];
        Users.findById(id, {
            attributes: fields
        }).then(user => {
            res.status(200).json(user);
        }).catch(err => {
            handleError(err, res);
        });
    });

    //add candidate if email not exists in db and then send email to user
    router.post("/", function (req, res) {
        const data = req.body.user;
        // console.log(data);
        var user = {
            "first_name": data.first_name,
            "family_name": data.family_name,
            "email": data.email,
            "password": pwHelper.getHash("1234"),
            "contact": data.contact,
            "role": 1
        }
        Users.findOrCreate({ where: { email: user.email }, defaults: user })
            .spread((u, created) => {
                if (created) {

                    const candidate = {
                        userId: u.id,
                        visastatus: data.visastatus,
                        experience: data.experience,
                        profile: data.profile,
                        degrees: data.degrees,
                        technical: data.technical,
                        functional: data.functional,
                        methodology: data.methodology,
                        management: data.management,
                        interviewer: data.interviewer,
                        file: data.file,
                        applyingfor: data.applyingfor,
                        travel: data.travel,
                        relocation: data.relocation,
                        typeofcontract: data.typeofcontract,
                        availability: data.availability,
                        current_fix: data.current_fix,
                        current_var: data.current_var,
                        expected: data.expected,
                        reasonsforchange: data.reasonsforchange
                    };

                    Candidates.create(candidate).then(created => {
                        console.log("Candidate data added");
                    }).catch(function (err) {
                        console.log(err);
                    });

                    // const link = "http://" + config.get("host") + "/verify/" + u.email + "/" + uuid();
                    var text = "Hi " + user.first_name + ",<p>We have received your registration. Thank you.</p>";
                    // text += "<p>Click on the following link to verify your account: "
                    // text += "<a href=" + link + ">" + link + "</a> </p>";
                    text += "You may login with the default password 1234";
                    text += "<p>If you did not make this request or believe this was received in error, please ignore this message.</p>";

                    var email = {
                        from: "donotreply@malterm.com",
                        to: u.email,
                        subject: "Thank you for your registration",
                        text: text,
                        html: text
                    };

                    mailgun.messages().send(email, function (error, body) {
                        if (error) {
                            console.log(error);
                            // console.log(body);
                            res.status(201).send("Your registration has been successfully submitted");
                        } else {
                            // console.log(body);
                            res.status(201).send("Your registration has been successfully submitted and an acknowledgement email sent to " + u.email);
                        }
                    });

                } else {
                    res.status(422).send("Could not complete registration as your email already exists in our system");
                }
            }).catch(err => {
                // console.log(err);
                handleError(err, res);
            });

    });

    //update particular user based on id
    router.put("/:id", function (req, res) {
        const id = parseInt(req.params.id); //to convert type 
        if (isNaN(id) || id < 0) {
            res.status(400).type("text/plain").send("id should be a (positive) number");
            return;
        }

        const user = req.body.user;

        Users.update(user, { where: { id: id } }).then(result => {
            res.status(200).send(result);
        }).catch(err => {
            // console.log(err);
            // res.status(500).send(err);
            handleError(err, res);
        });
    });

    //delete a particular user based on id
    router.delete("/:id", function (req, res) {
        const id = parseInt(req.params.id); //to convert type 
        // console.log(id);
        if (isNaN(id) || id < 0) {
            res.status(400).type("text/plain").send("id should be a (positive) number");
            return;
        }

        Users.destroy({ where: { id: id } }).then(result => {
            res.status(200).end();
        }).catch(function (err) {
            // console.log(err);
            // res.status(500).send(err);
            handleError(err, res);
        });
    });


    return router;
}