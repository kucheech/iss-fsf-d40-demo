const bCrypt = require("bcrypt-nodejs");

module.exports = {
    getHash: function (pw) {
        return bCrypt.hashSync(pw, bCrypt.genSaltSync(8), null);
    },
    
    isCorrect: function (pw1, pw2) {
        return bCrypt.compareSync(pw1, pw2);
    }
}