module.exports = function (err, res) {
    console.log(err);
    res.status(500).type("text/plain").send(JSON.stringify(err));
}