require('dotenv').config()

const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");
const fs = require("fs");
const multer = require("multer");
const uuid = require("uuid/v4");

const session = require("express-session");
const watch = require("connect-ensure-login");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;
const bCrypt = require("bcrypt-nodejs");
const config = require('config');
const pwHelper = require("./helpers/password");
const handleError = require("./helpers/handleError");


// function handleError(err, res) {
//     res.status(500).type("text/plain").send(JSON.stringify(err));
// }

const authenticate = function (username, password, done) {
    // console.log(username + ":" + password);
    // const valid = false;
    Users.findOne({
        where: {
            email: username,
            role: 1
        }
    }).then(found => {
        if (found && pwHelper.isCorrect(password, found.password)) {
            return done(null, username);
        } else {
            return done(null, false);
        }
    });
}

const authenticate2 = function (username, password, done) {
    // console.log(username + ":" + password);
    // const valid = false;
    Users.findOne({
        where: {
            email: username,
            role: 2
        }
    }).then(found => {
        if (found && pwHelper.isCorrect(password, found.password)) {
            return done(null, username);
        } else {
            return done(null, false);
        }
    });
}

passport.use("local1", new LocalStrategy({
    usernameField: "email",
    passwordField: "password"
}, authenticate));


passport.use("local2", new LocalStrategy({
    usernameField: "email",
    passwordField: "password"
}, authenticate2));



passport.serializeUser(function (username, done) {
    Users.findOne({ where: { email: username } }).then(found => {
        if (found) {
            done(null, found.id);
        }
    })
});

passport.deserializeUser(function (id, done) {
    // var userObject = {
    //     email: id
    // }
    // done(null, userObject);
    Users.findById(id).then(found => {
        if (found) {
            const user = {
                id: found.id
            }
            done(null, user);
        }
    })

});

// const mailgun = require("mailgun-js");
const mailgun = require('mailgun-js')({ apiKey: process.env.MG_API_KEY, domain: process.env.MG_DOMAIN });

//start sequelize
// const DB_NAME = "focus";
// const DB_USER = "root";
// const DB_PASSWORD = "password";
// const DB_HOST = "localhost"
const DB_NAME = process.env.DB_NAME;
const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_HOST = process.env.DB_HOST;
const Sequelize = require("sequelize");
const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD,
    {
        host: DB_HOST,
        dialect: 'mysql',
        logging: console.log,
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        define: {
            timestamps: true,

            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are enabled
            paranoid: true,

            // disable the modification of table names; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true,
        },
        // disable logging; default: console.log
        logging: false
    }
);

// sequelize
//     .authenticate()
//     .then(() => {
//         console.log('Connection has been established successfully.');
//     })
//     .catch(err => {
//         console.error('Unable to connect to the database:', err);
//     });

const Users = require("./models/user")(sequelize, Sequelize);
const Candidates = require("./models/candidate")(sequelize, Sequelize);
Candidates.belongsTo(Users, { foreignKey: "userId", targetKey: "id" });
// Users.hasOne(Candidates);
// const Verification = require("./models/verification")(sequelize, Sequelize);
const Jobs = require("./models/job")(sequelize, Sequelize);

var db = require("../models");
var Verification = db.Verification;


sequelize.sync({ force: true }).then(() => {

    var interviewer_ids = [];

    const interviewer1 = {
        "first_name": "William",
        "family_name": "Ku",
        "email": "wku@maltem.com",
        "password": pwHelper.getHash("1111"),
        "contact": "91788673",
        "role": 2
    };

    Users.create(interviewer1).then(created => {
        console.log("User " + created.first_name + " added");
    }).catch(function (err) {
        console.log(err);
    });

    const interviewer2 = {
        "first_name": "Kenneth",
        "family_name": "Phang",
        "email": "k@k.com",
        "password": pwHelper.getHash("1111"),
        "contact": "12345678",
        "role": 2
    };

    Users.create(interviewer2).then(created => {
        console.log("Interviewer " + created.first_name + " added");
        interviewer_ids.push(created.id);
    }).catch(function (err) {
        console.log(err);
    });

    const interviewer3 = {
        "first_name": "Vincent",
        "family_name": "Lau",
        "email": "v@v.com",
        "password": pwHelper.getHash("1111"),
        "contact": "12345678",
        "role": 2
    };

    Users.create(interviewer3).then(created => {
        console.log("Interviewer " + created.first_name + " added");
        interviewer_ids.push(created.id);
    }).catch(function (err) {
        console.log(err);
    });

    const candidate1 = {
        "first_name": "Alice",
        "family_name": "Hathaway",
        "email": "a@a.com",
        "password": pwHelper.getHash("1234"),
        "contact": "11111111",
        "role": 1
    };

    Users.create(candidate1).then(created => {
        const name = created.first_name;
        console.log("Candidate " + name + " added");
        const data1 = {
            userId: created.id,
            visastatus: "PR",
            experience: "5",
            profile: "Project Manager",
            degrees: "BSc(Computing)",
            technical: "C/C++",
            functional: "SDLC",
            methodology: null,
            management: "PMP",
            interviewer: interviewer_ids[0],
            applyingfor: "Consultant",
            interviewed: true
        };
        Candidates.create(data1).then(created => {
            console.log("Candidate data for " + name + " added");
        }).catch(function (err) {
            console.log(err);
        });
    }).catch(function (err) {
        console.log(err);
    });


    const candidate2 = {
        first_name: "Bob",
        family_name: "Hughes",
        email: "b@b.com",
        password: pwHelper.getHash("1234"),
        contact: "22222222",
        role: 1
    };

    Users.create(candidate2).then(created => {
        const name = created.first_name;
        console.log("Candidate " + name + " added");
        const data2 = {
            userId: created.id,
            visastatus: "Local resident",
            experience: "3",
            profile: "Software developer",
            degrees: "MSc(Computing)",
            technical: "Java, Javascript",
            functional: null,
            methodology: "Scrum",
            management: "PMP",
            interviewer: interviewer_ids[1],
            applyingfor: "ScrumMaster",
            interviewed: false
        };

        Candidates.create(data2).then(created => {
            console.log("Candidate data for " + name + " added");
        }).catch(function (err) {
            console.log(err);
        });


        const jobs = [
            { title: "Business Analyst" },
            { title: "Consultant" },
            { title: "Front-end Developer" },
            { title: "Back-end Developer" },
            { title: "Full-stack Developer" },
            { title: "ScrumMaster" }
        ];

        Jobs.bulkCreate(jobs).then(() => {
            console.log("bulkCreate " + jobs.length + " jobs");
        });

        // Verification.create({ id: "12dfa52e-06f9-442e-95e5-7436183af27d", email: "kucheech@gmail.com" }).then(created => { });

    }).catch(function (err) {
        console.log(err);
    });


}).catch(function (err) {
    console.log(err);
});

const app = express();
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ limit: "5mb", extended: true }));
app.use(bodyParser.json({ limit: "5mb" }));

app.use(session({
    secret: "iss-fsf",
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
app.use("/libs", express.static(BOWER_FOLDER));

app.use("/protected", watch.ensureLoggedIn("/status/401"));

// const POSTS_IMAGES_FOLDER = path.join(__dirname, "/images/posts/");
// app.use("/protected/posts/images/", express.static(POSTS_IMAGES_FOLDER));

// const AVATARS_FOLDER = path.join(__dirname, "/images/avatars/");
// app.use("/protected/avatars", express.static(AVATARS_FOLDER));

const STATIC_IMAGES_FOLDER = path.join(__dirname, "/images/static/");
app.use("/images/", express.static(STATIC_IMAGES_FOLDER));

const FILE_UPLOAD_FOLDER = path.join(__dirname, "/uploads/");
app.use("/uploads/", express.static(FILE_UPLOAD_FOLDER));


app.post("/login", passport.authenticate("local1", {
    successRedirect: "/status/202",
    failureRedirect: "/status/403"
}));

app.post("/login2", passport.authenticate("local2", {
    successRedirect: "/status/202",
    failureRedirect: "/status/403"
}));


app.get("/logout", function (req, res) {
    req.logout();
    req.session.destroy();
    res.status(200).end();
});

app.get("/status/:code", function (req, res) {
    const code = parseInt(req.params.code);
    res.sendStatus(code);
});

//all candidates
app.get("/protected/candidates/all", function (req, res) {
    Candidates.findAll({ order: [["createdAt", "ASC"]], include: [{ model: Users, attributes: ["id", "first_name", "family_name"] }] }).then(candidates => {
        res.status(200).json(candidates);
    })
});

//candidates for logged in interviewer
app.get("/protected/candidates", function (req, res) {
    Candidates.findAll({ where: { interviewer: req.user.id }, order: [["createdAt", "ASC"]], include: [{ model: Users, attributes: ["id", "first_name", "family_name"] }] }).then(candidates => {
        res.status(200).json(candidates);
    })
});


app.get("/protected/candidates/interviewer/:id", function (req, res) {
    const id = parseInt(req.params.id);
    Candidates.findAll({ where: { interviewer: id }, order: [["createdAt", "ASC"]], include: [{ model: Users, attributes: ["id", "first_name", "family_name"] }] }).then(candidates => {
        res.status(200).json(candidates);
    })
});

//get a particular candidate based on sender
app.get("/protected/candidate/me", function (req, res) {
    // console.log(req.user);
    const id = req.user.id;
    const fields = ["first_name", "family_name", "email", "contact"];

    Candidates.findOne({
        where: { userId: id },
        include: [{ model: Users, attributes: fields }]
    }).then(candidate => {
        res.status(200).json(candidate);
    }).catch(err => {
        handleError(err, res);
    });
});

//get a particular candidate based on id
app.get("/protected/candidates/:id", function (req, res) {
    const id = req.params.id;
    const fields = ["first_name", "family_name", "email", "contact"];

    Candidates.findOne({
        where: { userId: id },
        include: [{ model: Users, attributes: fields }]
    }).then(candidate => {
        res.status(200).json(candidate);
    }).catch(err => {
        handleError(err, res);
    });
});

//get a particular candidate based on email
app.get("/protected/candidates/email/:email", function (req, res) {
    console.log(req.user.username);
    const email = req.params.email;
    const fields = ["first_name", "family_name", "email", "contact", "role"];

    Users.findOne({ where: { email: email } }).then(user => {
        Candidates.findOne({ where: { userId: user.id }, include: [{ model: Users, attributes: fields }] }).then(candidate => {
            res.status(200).json(candidate);
        })
    }).catch(err => {
        handleError(err, res);
    });
});


//update particular candidate based on id
app.put("/protected/candidates/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    var candidate = req.body.candidate;
    // console.log(candidate);
    const user = candidate.user;
    delete candidate.user;

    Candidates.update(candidate, { where: { userId: id } }).then(result => {
        Users.update(user, { where: { id: id } }).then(result => {
            res.status(200).send("Changes saved successfully!");
        }).catch(err => {
            // console.log(err);
            // res.status(500).send(err);
            handleError(err, res);
        });

        // res.status(200).send(result);
    }).catch(err => {
        // console.log(err);
        // res.status(500).send(err);
        handleError(err, res);

    });

});


const storage = multer.diskStorage({
    destination: FILE_UPLOAD_FOLDER,
    filename: function (req, file, callback) {
        // console.log(req);
        // console.log(file);
        callback(null, Date.now() + "-" + file.originalname)
    }
});

const upload = multer({
    storage: storage
})

app.post("/uploads/", upload.single("pdf-file"), function (req, res) {
    // console.log(req);
    // console.log("upload");    
    const filename = uuid() + ".pdf";
    const filepath = path.resolve(FILE_UPLOAD_FOLDER, filename);
    fs.rename(req.file.path, filepath, function (err) {
        if (err) {
            console.log("Error: " + err);
        }

        res.status(202).send(filename);
    })
});


// Routes
app.use("/password", require("./routes/password")(Users, Verification, config, mailgun));
app.use("/jobs", require("./routes/jobs")(Jobs));
app.use("/interviewers", require("./routes/interviewers")(Users));
app.use("/validate", require("./routes/validate")(Users));
app.use("/users", require("./routes/users")(Users, Candidates, mailgun));


// //get all users
// app.get("/users", function (req, res) {
//     const fields = ["first_name", "family_name", "email", "contact", "role"];
//     Users.findAll({
//         attributes: fields
//     }
//     ).then(users => {
//         res.status(200).json(users);
//     })
// });

// //get a particular user based on id
// app.get("/users/:id", function (req, res) {
//     const id = req.params.id;
//     const fields = ["first_name", "family_name", "email", "contact", "role"];
//     Users.findById(id, {
//         attributes: fields
//     }).then(user => {
//         res.status(200).json(user);
//     }).catch(err => {
//         handleError(err, res);
//     });
// });

//verify an email
// app.get("/verify/:email/:hash", function (req, res) {
//     const email = req.params.email;
//     const hash = req.params.hash;
//     console.log(email + ", " + hash);
//     // Users.findById(id, {
//     //     attributes: fields
//     // }).then(user => {
//     //     res.status(200).json(user);
//     // }).catch(err => {
//     //     handleError(err, res);
//     // });
// });


// //add candidate if email not exists in db and then send email to user
// app.post("/users", function (req, res) {
//     const data = req.body.user;
//     // console.log(data);
//     var user = {
//         "first_name": data.first_name,
//         "family_name": data.family_name,
//         "email": data.email,
//         "password": pwHelper.getHash("1234"),
//         "contact": data.contact,
//         "role": 1
//     }
//     Users.findOrCreate({ where: { email: user.email }, defaults: user })
//         .spread((u, created) => {
//             if (created) {

//                 const candidate = {
//                     userId: u.id,
//                     visastatus: data.visastatus,
//                     experience: data.experience,
//                     profile: data.profile,
//                     degrees: data.degrees,
//                     technical: data.technical,
//                     functional: data.functional,
//                     methodology: data.methodology,
//                     management: data.management,
//                     interviewer: data.interviewer,
//                     file: data.file,
//                     applyingfor: data.applyingfor,
//                     travel: data.travel,
//                     relocation: data.relocation,
//                     typeofcontract: data.typeofcontract,
//                     availability: data.availability,
//                     current_fix: data.current_fix,
//                     current_var: data.current_var,
//                     expected: data.expected,
//                     reasonsforchange: data.reasonsforchange
//                 };

//                 Candidates.create(candidate).then(created => {
//                     console.log("Candidate data added");
//                 }).catch(function (err) {
//                     console.log(err);
//                 });

//                 // const link = "http://" + config.get("host") + "/verify/" + u.email + "/" + uuid();
//                 var text = "Hi " + user.first_name + ",<p>We have received your registration. Thank you.</p>";
//                 // text += "<p>Click on the following link to verify your account: "
//                 // text += "<a href=" + link + ">" + link + "</a> </p>";
//                 text += "<p>If you did not make this request or believe this was received in error, please ignore this message.</p>";

//                 var email = {
//                     from: "donotreply@malterm.com",
//                     to: u.email,
//                     subject: "Thank you for your registration",
//                     text: text,
//                     html: text
//                 };

//                 mailgun.messages().send(email, function (error, body) {
//                     if (error) {
//                         console.log(error);
//                         // console.log(body);
//                         res.status(201).send("Your registration has been successfully submitted");
//                     } else {
//                         // console.log(body);
//                         res.status(201).send("Your registration has been successfully submitted and an acknowledgement email sent to " + u.email);
//                     }
//                 });

//             } else {
//                 res.status(422).send("Could not complete registration as your email already exists in our system");
//             }
//         }).catch(err => {
//             // console.log(err);
//             handleError(err, res);
//         });

// });

// //update particular user based on id
// app.put("/users/:id", function (req, res) {
//     const id = parseInt(req.params.id); //to convert type 
//     if (isNaN(id) || id < 0) {
//         res.status(400).type("text/plain").send("id should be a (positive) number");
//         return;
//     }

//     const user = req.body.user;

//     Users.update(user, { where: { id: id } }).then(result => {
//         res.status(200).send(result);
//     }).catch(err => {
//         // console.log(err);
//         // res.status(500).send(err);
//         handleError(err, res);
//     });
// });

// //delete a particular user based on id
// app.delete("/users/:id", function (req, res) {
//     const id = parseInt(req.params.id); //to convert type 
//     // console.log(id);
//     if (isNaN(id) || id < 0) {
//         res.status(400).type("text/plain").send("id should be a (positive) number");
//         return;
//     }

//     Users.destroy({ where: { id: id } }).then(result => {
//         res.status(200).end();
//     }).catch(function (err) {
//         // console.log(err);
//         // res.status(500).send(err);
//         handleError(err, res);
//     });
// });


//delete a particular candidate data based on id
app.delete("/protected/candidate/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    // console.log(id);
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    Candidates.destroy({ where: { userId: id } }).then(result => {
        res.status(200).end();
    }).catch(function (err) {
        // console.log(err);
        // res.status(500).send(err);
        handleError(err, res);
    });
});


app.get("/homepageimages", function (req, res) {
    var images = [
        "hp01.jpg",
        "hp02.jpg",
        "hp03.jpg",
        "hp04.jpg",
        "hp05.jpg"
    ];
    res.status(200).json(images);
});


// fix for html5enable mode
app.all('/*', function (req, res, next) {
    res.sendFile('index.html', { root: CLIENT_FOLDER });
});

//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});


//make the app public. In this case, make it available for the testing platform
module.exports = app