module.exports = function (sequelize, Sequelize) {
    const Candidate = sequelize.define("candidates",
        {
            // id: {
            //     type: Sequelize.INTEGER,  
            //     primaryKey: true,              
            //     allowNull: false,
            //     autoIncrement: true
            // },
            userId: {
                type: Sequelize.INTEGER,  
                primaryKey: true,              
                allowNull: false
            },
            visastatus: {
                type: Sequelize.STRING,
                allowNull: false
            },
            experience: {
                type: Sequelize.STRING,
                allowNull: false
            },
            profile: {
                type: Sequelize.STRING,
                allowNull: false
            },
            degrees: {
                type: Sequelize.STRING,
                allowNull: false
            },
            technical: {
                type: Sequelize.STRING,
                allowNull: true
            },
            functional: {
                type: Sequelize.STRING,
                allowNull: true
            },
            methodology: {
                type: Sequelize.STRING,
                allowNull: true
            },
            management: {
                type: Sequelize.STRING,
                allowNull: true
            },
            file: {
                type: Sequelize.STRING,
                allowNull: true
            },
            interviewer: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            applyingfor: {
                type: Sequelize.STRING,
                allowNull: true
            },
            travel: {
                type: Sequelize.BOOLEAN,
                defaultValue: true,
                allowNull: true
            },
            relocation: {
                type: Sequelize.BOOLEAN,
                defaultValue: true,
                allowNull: true
            },
            typeofcontract: {
                type: Sequelize.STRING,
                allowNull: true
            },
            availability: {
                type: Sequelize.STRING,
                allowNull: true
            },
            current_fix: {
                type: Sequelize.STRING,
                allowNull: true
            },
            current_var: {
                type: Sequelize.STRING,
                allowNull: true
            },
            expected: {
                type: Sequelize.STRING,
                allowNull: true
            },
            reasonsforchange: {
                type: Sequelize.STRING,
                allowNull: true
            },
            interviewed: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
                allowNull: false
            },
            notes: {
                type: Sequelize.STRING,
                allowNull: true
            },
            status: {
                type: Sequelize.ENUM("proceed", "kiv", "stop"),
                allowNull: true
            }
        }, {
            // tableName: "candidates"
            // freezeTableName: true            
        });

    return Candidate;
};