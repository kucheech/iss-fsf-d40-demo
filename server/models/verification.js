module.exports = function (sequelize, Sequelize) {
    const Verification = sequelize.define("verification",
        {
            email: {
                type: Sequelize.STRING,
                allowNull: false
            },
            id: {
                type: Sequelize.STRING,
                primaryKey: true,
                allowNull: false
            }
        }, {
            // tableName: "users"
            // freezeTableName: true            
        });

    return Verification;
};