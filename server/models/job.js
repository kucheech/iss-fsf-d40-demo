module.exports = function (sequelize, Sequelize) {
    const Job = sequelize.define("jobs",
        {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            title: {
                type: Sequelize.STRING,
                allowNull: false
            },
            description: {
                type: Sequelize.STRING,
                allowNull: true
            }
        }, {
            // tableName: "users"
            // freezeTableName: true            
        });

    return Job;
};