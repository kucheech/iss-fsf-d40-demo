module.exports = function (sequelize, Sequelize) {
    const User = sequelize.define("users",
        {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            first_name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            family_name: {
                type: Sequelize.STRING,
                allowNull: true
            },
            email: {
                type: Sequelize.STRING,
                allowNull: false
            },
            contact: {
                type: Sequelize.STRING,
                allowNull: true
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false
            },
            role: {
                type: Sequelize.ENUM("candidate", "interviewer"),
                defaultValue: "candidate",
            },
            verified: {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            }
        }, {
            // tableName: "users"
            // freezeTableName: true            
        });

    return User;
};