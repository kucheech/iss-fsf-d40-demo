(function () {
    'use strict';

    angular
        .module('MyApp')
        .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ["MyAppService"];
    function HomeCtrl(MyAppService) {
        var vm = this;
        vm.slides = [];

        init();

        //////////////////////////

        function init() {
            MyAppService.getHomepageImages().then(function (result) {
                // console.log(result);
                vm.slides = result;
            }).catch(function (err) {
                console.log(err);
            });
        }
    }
})();