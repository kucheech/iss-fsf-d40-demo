(function () {
    "use strict";
    angular.module("MyApp").controller("ResetPasswordCtrl", ResetPasswordCtrl);

    ResetPasswordCtrl.$inject = ["MyAppService", "$state", "$timeout"];

    function ResetPasswordCtrl(MyAppService, $state, $timeout) {
        var vm = this; // vm
        vm.email = "";
        vm.status = false;
        vm.message = "";
        vm.showMessage = false;

        vm.resetPassword = resetPassword;
        vm.cancel = cancel;

        /////////////////////////////////

        function cancel() {
            $state.go("login");
        }

        function resetPassword() {
            vm.showMessage = false;

            MyAppService.resetPassword(vm.email)
                .then(function (result) {
                    // console.log(result);
                    // $state.go("accounts");
                    if (result.status == 200) {
                        vm.status = true;
                        vm.message = result.data;
                        vm.showMessage = true;

                        $timeout(function () {
                            $state.go("home");
                        }, 2000);
                    }
                }).catch(function (err) {
                    console.log(err);
                    vm.status = false;
                    vm.message = err.data;
                    vm.showMessage = true;
                });
        }

    }

})();