(function () {
    "use strict";
    angular.module("MyApp").controller("ChangePasswordCtrl", ChangePasswordCtrl);

    ChangePasswordCtrl.$inject = ["MyAppService", "$state", "$timeout"];

    function ChangePasswordCtrl(MyAppService, $state, $timeout) {
        var vm = this; // vm
        vm.password1 = "";
        vm.password2 = "";
        vm.status = false;
        vm.message = "";
        vm.showMessage = false;

        vm.changePassword = changePassword;
        vm.cancel = cancel;

        init();

        ///////////////////////////////

        function init() {
            vm.id = $state.params.id;
        }

        function cancel() {
            $state.go("home");
        }

        function changePassword() {
            vm.showMessage = false;

            const user = {
                id: vm.id,
                password: vm.password1
            }

            MyAppService.changePassword(user)
                .then(function (result) {
                    // console.log(result);
                    if (result.status == 200) {
                        vm.status = true; //disables the Save button
                        vm.message = result.data;
                        vm.showMessage = true;

                        $timeout(function () {
                            $state.go("home");
                        }, 2000);
                    }
                }).catch(function (err) {
                    console.log(err);
                    vm.status = false;
                    vm.message = err.data;
                    vm.showMessage = true;
                });

        }

    }

})();