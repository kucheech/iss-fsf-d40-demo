(function () {
    "use strict";
    angular.module("MyApp").controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["MyAppService", "$state", "$timeout"];

    function EditCtrl(MyAppService, $state, $timeout) {
        var vm = this; // vm
        vm.candidate = null;
        vm.editPersonalInformationMode = false;
        vm.editCompetenciesMode = false;
        vm.id;
        vm.cancelEdit = cancelEdit;
        vm.saveEdit = saveEdit;
        vm.editPersonalInformation = editPersonalInformation;
        vm.editCompetencies = editCompetencies;
        vm.showFilePanel = showFilePanel;
        vm.status = false;
        vm.message = "";
        vm.showMessage = false;

        init();

        /////////////////////////////////

        function showFilePanel() {
            // console.log(vm.candidate.file);
            if (vm.candidate == null) {
                return false;
            }
            return vm.candidate.file != null;
        }

        function editPersonalInformation() {
            vm.editPersonalInformationMode = !vm.editPersonalInformationMode;
        }

        function editCompetencies() {
            vm.editCompetenciesMode = !vm.editCompetenciesMode;
        }


        function init() {
            // console.log($state.params.id);
            // vm.id = $state.params.email;
            // vm.showResults = false;

            MyAppService.getMyselfCandidate()
                .then(function (result) {
                    // console.log(result);
                    vm.candidate = result;
                }).catch(function (err) {
                    console.log(err);
                });

        }

        function cancelEdit() {
            // $state.go("home");
            logout();
        }

        function saveEdit() {
            vm.candidate.interviewed = true;
            
            MyAppService.updateCandidate(vm.candidate)
                .then(function (result) {
                    // console.log(result);
                    // $state.go("home");
                    vm.status = true;
                    vm.message = result.data;
                    vm.showMessage = true;

                    $timeout(function () {
                        logout();
                    }, 3000);
                    
                }).catch(function (err) {
                    console.log(err)
                });
        }

        function logout() {
            // console.log("ctrl login");
            MyAppService.logout()
                .then(function (result) {
                    console.log(result);
                    $state.go("home");
                }).catch(function (err) {
                    console.log(err);
                });
        }

    }

})();