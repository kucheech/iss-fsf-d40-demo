(function () {
    "use strict";
    angular.module("MyApp").controller("RegisterCtrl", RegisterCtrl);

    RegisterCtrl.$inject = ["MyAppService", "$state", "$timeout"];

    function RegisterCtrl(MyAppService, $state, $timeout) {
        var vm = this; // vm
        vm.progress = 0;
        vm.interviewers = [];
        vm.interviewer = null;
        vm.user = {
            name: "",
            email: "",
            travel: true,
            relocation: true
        };
        vm.status = false;
        vm.message = "";
        vm.showMessage = false;
        vm.file = "";
        vm.jobs = [];
        vm.visas = ["Singaporean", "PR", "Malaysian", "Foreigner", "EP Holder"];
        vm.availabilitytypes = ["Immediate", "1 month", "2 months"];

        vm.setProgress = setProgress
        vm.setProgress2 = setProgress2
        // vm.incProgress = incProgress
        vm.cancel = cancel;
        vm.register = register;
        vm.showPage = showPage;
        vm.validateEmail = validateEmail;
        vm.gotoPage = gotoPage;
        vm.selectedItem = null;
        vm.selectedItem2 = null;
        vm.selectedItem3 = null;
        vm.searchText = "";
        vm.searchText2 = "";
        vm.searchText3 = "";
        vm.querySearch = querySearch;
        vm.querySearch2 = querySearch2;
        vm.querySearch3 = querySearch3;

        init();

        /////////////////////////

        function gotoPage(p, next) {
            switch (parseInt(p)) {
                case 0: vm.progress = (next ? 0 : 0); return;
                case 1: vm.progress = (next ? 5 : 8); return;
                case 2: vm.progress = (next ? 10 : 27.5); return;
                case 3: vm.progress = (next ? 30 : 55); return;
                case 4: vm.progress = (next ? 60 : 80); return;
                case 5: vm.progress = (next ? 80 : 89); return;
                case 6: vm.progress = 90; return;
            }
        }

        function validateEmail() {
            vm.message = null;
            vm.showMessage = false;

            MyAppService.validateEmail(vm.user.email)
                .then(function (result) {
                    // console.log(result);
                    if (result.status == 200) {
                        vm.progress = 5;
                    }
                }).catch(function (err) {
                    // console.log(err);
                    if (err.status == 302) {
                        vm.message = err.data;
                        vm.showMessage = true;
                    }

                });
        }

        function showPage(p) {
            switch (parseInt(p)) {
                case 0: return vm.progress == 0;
                case 1: return vm.progress > 0 && vm.progress < 10;
                case 2: return vm.progress >= 10 && vm.progress < 30;
                case 3: return vm.progress >= 30 && vm.progress < 60
                case 4: return vm.progress >= 60 && vm.progress < 80;
                case 5: return vm.progress >= 80 && vm.progress < 90;
                case 6: return vm.progress >= 90;
            }
        }

        function setProgress2(n) {
            vm.progress = n;
        }

        function setProgress(n) {
            // progress is only incremented
            if (n > vm.progress) {
                vm.progress = n;
            }
            // console.log(vm.progress);
        }

        // function incProgress(max) {
        //     // progress is only incremented
        //     if (vm.progress < max) {
        //         vm.progress += 2.5;
        //     }
        //     // console.log(vm.progress);
        // }

        function init() {
            MyAppService.getInterviewers()
                .then(function (result) {
                    vm.interviewers = result;
                    // console.log(result);  
                    vm.user.interviewer = 0;
                }).catch(function (err) {
                    console.log(err);
                });

            MyAppService.getJobTitles().then(function (result) {
                // console.log(result);
                vm.jobs = result;
            }).catch(function (err) {
                console.log(err);
            });
        }

        function cancel() {
            $state.go("home");
        }

        function register() {
            vm.showMessage = false;

            MyAppService.submitRegistration(vm.user, vm.file)
                .then(function (result) {
                    console.log(result);
                    if (result.status == 201) {
                        vm.status = true;
                        vm.message = result.data;
                        vm.showMessage = true;

                        $timeout(function () {
                            $state.go("home");
                        }, 3000);
                    }
                    // $state.go("login");
                }).catch(function (err) {
                    console.log(err);
                    vm.status = false;
                    vm.message = err.data;
                    vm.showMessage = true;
                });
        }

        // https://material.angularjs.org/latest/demo/autocomplete
        function querySearch(query) {
            // console.log(query);
            var results = query ? vm.jobs.filter(createFilterFor(query)) : vm.jobs;
            // console.log(results);
            return results;
        }

        //  Create filter function for a query string
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(job) {
                // console.log(job);
                return (job.title.toLowerCase().indexOf(lowercaseQuery) >= 0);
            };

        }

        function querySearch2(query) {
            // console.log(query);
            var results = query ? vm.visas.filter(createFilterFor2(query)) : vm.visas;
            // console.log(results);
            return results;
        }

        //  Create filter function for a query string
        function createFilterFor2(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(visa) {
                // console.log(job);
                return (visa.toLowerCase().indexOf(lowercaseQuery) >= 0);
            };

        }


        function querySearch3(query) {
            // console.log(query);
            var results = query ? vm.availabilitytypes.filter(createFilterFor3(query)) : vm.availabilitytypes;
            // console.log(results);
            return results;
        }

        //  Create filter function for a query string
        function createFilterFor3(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(avail) {
                return (avail.toLowerCase().indexOf(lowercaseQuery) >= 0);
            };

        }

    }

})();