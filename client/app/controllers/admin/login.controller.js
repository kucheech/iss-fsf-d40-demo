(function () {
    'use strict';

    angular
        .module('MyApp')
        .controller('InterviewerLoginCtrl', InterviewerLoginCtrl);

    InterviewerLoginCtrl.$inject = ["$scope", "MyAppService", "$state", "authService"];

    function InterviewerLoginCtrl($scope, MyAppService, $state, authService) {
        var vm = this;
        vm.user = {
            email: "",
            password: ""
        };
        vm.message = "";
        vm.showMessage = false;

        vm.login = login;

        ///////////////////////

        $scope.$on("event:auth-loginRequired", function () {
            // console.log("401");
            $state.go("login");
        });

        $scope.$on("event:auth-loginConfirmed", function () {
            // console.log("202");
            $state.go("candidates");
        });

        $scope.$on("event:auth-forbidden", function () {
            console.log("403");
        });

        //////////////////////////

        function login() {
            // console.log("ctrl login");
            vm.showMessage = false;

            MyAppService.login2(vm.user)
                .then(function (result) {
                    // console.log(result);
                    if (result.status == 202) {
                        authService.loginConfirmed();
                    }
                }).catch(function (err) {
                    console.log(err);
                    vm.message = err.status + " " + err.statusText;
                    vm.showMessage = true;
                });
        }

    }
})();