(function () {
    "use strict";
    angular.module("MyApp").controller("InterviewCtrl", InterviewCtrl);

    InterviewCtrl.$inject = ["MyAppService", "$state"];

    function InterviewCtrl(MyAppService, $state) {
        var vm = this; // vm
        vm.candidate = null;
        vm.editPersonalInformationMode = false;
        vm.editCompetenciesMode = false;
        vm.id;
        vm.cancelEdit = cancelEdit;
        vm.saveEdit = saveEdit;
        vm.editPersonalInformation = editPersonalInformation;
        vm.editCompetencies = editCompetencies;
        vm.showFilePanel = showFilePanel;
        init();

        ///////////////////////////

        function showFilePanel() {
            // console.log(vm.candidate.file);
            if (vm.candidate == null) {
                return false;
            }
            return vm.candidate.file != null;
        }

        function editPersonalInformation() {
            vm.editPersonalInformationMode = !vm.editPersonalInformationMode;
        }

        function editCompetencies() {
            vm.editCompetenciesMode = !vm.editCompetenciesMode;
        }


        function init() {
            // console.log($state.params.id);
            vm.id = $state.params.id;
            // vm.showResults = false;

            MyAppService.getCandidateById(vm.id)
                .then(function (result) {
                    console.log(result);
                    vm.candidate = result;
                }).catch(function (err) {
                    console.log(err);
                });

        }

        function cancelEdit() {
            $state.go("candidates");
        }

        function saveEdit() {
            vm.candidate.interviewed = true;
            
            MyAppService.updateCandidate(vm.candidate)
                .then(function (result) {
                    // console.log(result);
                    $state.go("candidates");
                }).catch(function (err) {
                    console.log(err)
                });
        }

    }

})();