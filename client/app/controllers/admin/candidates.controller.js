(function () {
    "use strict";

    angular
        .module("MyApp")
        .controller("CandidatesCtrl", CandidatesCtrl);

    CandidatesCtrl.$inject = ["$scope", "MyAppService", "$state", "authService"];
    function CandidatesCtrl($scope, MyAppService, $state, authService) {
        var vm = this;
        vm.candidates = [];
        vm.viewAllCandidates = false;
        vm.logout = logout;
        vm.interview = interview
        vm.getCandidates = getCandidates;

        init();
        ///////////////////////

        $scope.$on("event:auth-loginRequired", function () {
            console.log("401");
            $state.go("login2");
        });

        $scope.$on("event:auth-loginConfirmed", function () {
            console.log("202");
            $state.go("candidates");
        });

        $scope.$on("event:auth-forbidden", function () {
            console.log("403");
            $state.go("home");
        });


        /////////////////////////////////////

        function getCandidates() {

            MyAppService.getCandidates(vm.viewAllCandidates)
                .then(function (result) {
                    // console.log(result);
                    vm.candidates = result;
                }).catch(function (err) {
                    console.log(err);
                });
        }



        function interview(id) {
            // console.log(id);
            $state.go("interview", { id: id });
        }

        function init() {
            getCandidates(vm.viewAllCandidates);
        }

        function logout() {
            // console.log("ctrl login");
            MyAppService.logout()
                .then(function (result) {
                    console.log(result);
                    $state.go("home");
                }).catch(function (err) {
                    console.log(err);
                });
        }
    }

})();