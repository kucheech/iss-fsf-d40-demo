(function () {
    angular.module("MyApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q", "$httpParamSerializerJQLike", "Upload"];

    function MyAppService($http, $q, $httpParamSerializerJQLike, Upload) {
        var service = this;

        //expose the following services
        service.login = login;
        service.logout = logout;
        // service.getProtectedAccounts = getProtectedAccounts;
        service.resetPassword = resetPassword;
        service.login2 = login2;
        service.getCandidates = getCandidates;
        service.getCandidateById = getCandidateById;
        service.getMyselfCandidate = getMyselfCandidate;
        service.getInterviewers = getInterviewers;
        service.submitRegistration = submitRegistration;
        service.validateEmail = validateEmail;
        service.updateCandidate = updateCandidate;
        service.getHomepageImages = getHomepageImages;
        service.getJobTitles = getJobTitles;
        service.changePassword = changePassword;
        

        ////////////////////////////////////


        
        function changePassword(user) {
            // console.log("service login: " + user);
            var defer = $q.defer();
            // $http.post("/changepassword", user)
            $http.post("/password/change", user)
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getJobTitles() {
            var defer = $q.defer();

            $http.get("/jobs/title").then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function getHomepageImages() {
            var defer = $q.defer();

            $http.get("/homepageimages").then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function validateEmail(email) {
            var defer = $q.defer();

            $http.get("/validate/email/" + email).then(function (result) {
                // console.log(result);
                defer.resolve(result);
            }).catch(function (err) {
                // console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function getInterviewers() {
            var defer = $q.defer();

            $http.get("/interviewers").then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function login2(user) {
            // console.log("service login: " + user);
            var defer = $q.defer();
            $http.post("/login2", user)
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }


        function getMyselfCandidate() {
            var defer = $q.defer();

            $http.get("/protected/candidate/me").then(function (result) {
                console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function getCandidateById(id) {
            var defer = $q.defer();

            $http.get("/protected/candidates/" + id).then(function (result) {
                // console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function getCandidates(all) {
            var defer = $q.defer();
            var url = "/protected/candidates";
            if (all) {
                url += "/all";
            }

            $http.get(url).then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        // function submitPost(post, file) {
        //     var defer = $q.defer();
        //     if (file != "") {
        //         uploadImage(file).then(function (result) {
        //             // console.log(result);
        //             post.image = result;
        //             defer.resolve(createPost(post));
        //         }).catch(function (err) {
        //             console.log(err)
        //         });
        //     } else {
        //         defer.resolve(createPost(post));
        //     }

        //     return defer.promise;
        // }

        // function uploadImage(file) {
        //     // console.log("uploadImage");
        //     var defer = $q.defer();
        //     Upload.upload({
        //         url: "/protected/upload",
        //         data: {
        //             "img-file": file
        //         }
        //     }).then(function (result) {
        //         // console.log(result);
        //         defer.resolve(result.data);
        //     }).catch(function (err) {
        //         console.log(err);
        //         defer.reject(err);
        //     });

        //     return defer.promise;
        // }

        function submitRegistration(user, file) {
            // console.log(file);
            var defer = $q.defer();
            if (file != "") {
                uploadPDF(file).then(function (result) {
                    // console.log(result);
                    user.file = result;
                    defer.resolve(createUser(user));
                }).catch(function (err) {
                    console.log(err)
                });
            } else {
                // console.log("not uploading file...");
                defer.resolve(createUser(user));
            }

            return defer.promise;
        }


        function uploadPDF(file) {
            var defer = $q.defer();
            Upload.upload({
                url: "/uploads/",
                data: {
                    "pdf-file": file
                }
            }).then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function createUser(user) {
            var defer = $q.defer();

            $http.post("users", { user: user })
                .then(function (result) {
                    console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }



        // function createPost(post) {
        //     var defer = $q.defer();

        //     $http.post("/protected/posts", { post: post })
        //         .then(function (result) {
        //             // console.log(result);
        //             defer.resolve(result);
        //         }).catch(function (err) {
        //             defer.reject(err);
        //         });

        //     return defer.promise;
        // }


        // function getPosts() {
        //     var defer = $q.defer();

        //     $http.get("/protected/posts").then(function (result) {
        //         // console.log(result);
        //         defer.resolve(result.data);
        //     }).catch(function (err) {
        //         console.log(err);
        //         defer.reject(err);
        //     });

        //     return defer.promise;
        // }


        function resetPassword(email) {
            var defer = $q.defer();
            // $http.post("/resetpassword", { email: email })
            $http.post("/password/reset", { email: email })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getProtectedAccounts() {
            var defer = $q.defer();

            $http.get("/protected/accounts").then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function logout() {
            var defer = $q.defer();

            $http.get("/logout").then(function (result) {
                console.log(result);
                defer.resolve(result);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function login(user) {
            // console.log("service login: " + user);
            var defer = $q.defer();
            $http.post("/login", user)
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        // function createAccount(account) {
        //     var defer = $q.defer();

        //     $http.post("/accounts", { account: account })
        //         .then(function (result) {
        //             // console.log(result);
        //             defer.resolve(result);
        //         }).catch(function (err) {
        //             defer.reject(err);
        //         });

        //     return defer.promise;
        // }


        function updateCandidate(candidate) {
            var defer = $q.defer();
            const id = candidate.userId;
            $http.put("/protected/candidates/" + id, { candidate: candidate })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        // function getAccountById(id) {
        //     var defer = $q.defer();

        //     $http.get("/accounts/" + id).then(function (result) {
        //         // console.log(result);
        //         if (result.status == 200) {
        //             defer.resolve(result.data);
        //         } else {
        //             defer.resolve(null);
        //         }

        //     }).catch(function (err) {
        //         console.log(err);
        //         defer.reject(err);
        //     });

        //     return defer.promise;
        // }

        // function getAccounts() {
        //     var defer = $q.defer();

        //     $http.get("/accounts").then(function (result) {
        //         console.log(result);
        //         defer.resolve(result.data);
        //     }).catch(function (err) {
        //         console.log(err);
        //         defer.reject(err);
        //     });

        //     return defer.promise;
        // }

    }

})();