(function () {
    "use strict";
    angular.module("MyApp").config(MyConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider", "$locationProvider"];

    function MyConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $stateProvider
            .state("home", {
                url: "/",
                templateUrl: "/views/home.html",
                controller: "HomeCtrl",
                controllerAs: "ctrl"
            })
            .state("login1", {
                url: "/login",
                templateUrl: "/views/candidate/login.html",
                controller: "LoginCtrl",
                controllerAs: "ctrl"
            })
            .state("login2", {
                url: "/admin/login",
                templateUrl: "/views/admin/login.html",
                controller: "InterviewerLoginCtrl",
                controllerAs: "ctrl"
            })
            // .state("protected", {
            //     url: "/protected",
            //     templateUrl: "/views/protected.html",
            //     controller: "ProtectedCtrl",
            //     controllerAs: "ctrl"
            // })
            .state("candidates", {
                url: "/admin/candidates",
                templateUrl: "/views/admin/candidates.html",
                controller: "CandidatesCtrl",
                controllerAs: "ctrl"
            })
            .state("edit", {
                url: "/edit",
                templateUrl: "/views/candidate/edit.html",
                controller: "EditCtrl",
                controllerAs: "ctrl"
            })
            .state("interview", {
                url: "/interview/:id",
                templateUrl: "/views/admin/interview.html",
                controller: "InterviewCtrl",
                controllerAs: "ctrl"
            })
            .state("register", {
                url: "/register",
                templateUrl: "/views/candidate/register.html",
                controller: "RegisterCtrl",
                controllerAs: "ctrl"
            })
            .state("resetpassword", {
                url: "/resetpassword",
                templateUrl: "/views/resetpassword.html",
                controller: "ResetPasswordCtrl",
                controllerAs: "ctrl"
            })
            .state("changepassword", {
                url: "/changepassword/:id",
                templateUrl: "/views/changepassword.html",
                controller: "ChangePasswordCtrl",
                controllerAs: "ctrl"
            })

        $urlRouterProvider.otherwise("/");
    }

})();