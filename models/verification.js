'use strict';
module.exports = (sequelize, DataTypes) => {
  var Verification = sequelize.define('Verification', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
      classMethods: {
        associate: function (models) {
          // associations can be defined here
        }
      }
    });
  return Verification;
};