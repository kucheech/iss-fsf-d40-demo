### server unit testing
1. ```npm install --save-dev babel-cli babel-preset-env jest supertest superagent```
1. change test in package.json to jest
1. ```npm test```

### Kill node window process
http://www.wisdomofjim.com/blog/how-kill-running-nodejs-processes-in-windows
```taskkill /im node.exe /F```

### auth local
```npm install passport passport-local body-parser express-session connect-ensure-login --save```

### HTTP Auth Interceptor Module
https://github.com/witoldsz/angular-http-auth
```bower install --save angular-http-auth```

### heroku link
KIV

### AWS link
http://54.169.45.163/

### local run
1. ```npm install```
1. Create a .env file with the following environment variables
```
DB_NAME = "focus"
DB_USER = <removed>
DB_PASSWORD = <removed>
DB_HOST = "localhost"

MG_API_KEY = <removed>
MG_DOMAIN = <removed>
```

### AngularJS Material
https://material.angularjs.org/latest/getting-started
https://github.com/angular/bower-material
```bower install angular-material --save```

###Sequelize CLI
https://github.com/sequelize/cli

CLI v3 fully supports Sequelize v3. Support for Sequelize v4 is still experimental.
