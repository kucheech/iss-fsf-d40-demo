"use strict";
const request = require('supertest');
const app = require('../server/app');

/* test root path */
describe('Test the root path', function() {
    test('It should response the GET method', function() {
        return request(app).get("/").then(function(response) {
            expect(response.statusCode).toBe(200);
        })
    });
});

/** test no end point found */
describe('Test no end point found', function() {
    test('It return page not found in response', function() {
        return request(app).get("/sdffdf").then(function(response) {
            // expect(response.statusCode).toBe(404);
            expect(response.statusCode).toBe(200); //due to html5enable mode
        })
    });
});

// /** test /friends endpoint */
// describe('Test /users endpoint', function() {
//     test('It should return an array', function() {
//         return request(app).get("/users").then(function(response) {
//             expect(response.statusCode).toBe(200);
//             expect(response.type).toBe("application/json");
//             var array = response.body;
//             // console.log(JSON.stringify(response.body));
//             expect(response.body.length).toBe(2);
//         })
//     });
// });


// /** test /numfriends endpoint */
// describe('Test /numusers endpoint', function() {
//     test('It should return a value 2', function() {
//         return request(app).get("/numusers").then(function(response) {
//             expect(response.statusCode).toBe(200);
//             expect(response.text).toBe("2");
//             // console.log(response);
//         })
//     });
// });

//test /jobs/title endpoint
describe('Test /jobs/title endpoint', function() {
    test('It should return an array', function() {
        return request(app).get("/jobs/title").then(function(response) {
            expect(response.statusCode).toBe(200);
            expect(response.type).toBe("application/json");
            var array = response.body;
            // console.log(JSON.stringify(response.body));
            // expect(response.body.length).toBe(6);
        })
    });
});